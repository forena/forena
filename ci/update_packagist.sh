#!/usr/bin/env bash
set -e
curl -XPOST -H 'content-type:application/json' \
 "https://packagist.org/api/update-package?username=metzlerdl&apiToken=$PACKAGIST_API_TOKEN" \
 -d '{"repository":{"url":"git@gitlab.com:forena/forena.git"}}'
