#!/usr/bin/env bash

# Install the package with composer
set -xe
composer install --no-progress
# Run unit tests.
vendor/bin/phpunit --configuration tests/phpunit.xml

