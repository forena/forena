# Forena
Forena is an MVC framework for rapidly building web applications.  It was inspired
by a report writer developed for Drupal under the same name, but is being rewritten as a Symfony component 
for use in the broader community.

The current production version of the project is implemented as a module in the 
Drupal content management system.  Work is underway to port this into an application
independent framework which would still be used under drupal be also be 
available to the broader PHP community. 

See the [ Product Roadmap ](Roadmap.md) for information on future features. 

# Installation
Install the library via composer. 

````
composer require forena/forena
````

#Using the Forena
## Model
Forena's model is designed to be data platform agnostic.  It will work equally 
well with database technologies, json web services or XML data sources. 

Aribitratry data can be added to Forena data contexts  via the Data service.  The 
following illustrates basic usage: 

````
use Forena\Data\DataService

$service = DataService::service(); 

// Add data to the current data context
$service->addContext($data, 'mydata'); 
````
In the above example the variable $data could either be an object, an array of 
objects, a SimpleXMLElement, or an array of unstructured data. 

## View
Create new elements directly in your code: 

````
// Basic HTML Creation. 
$view = Div::tag(['class' => 'cool'])
  ->addText("Hello world.")
  ->show();
$this->assertEquals("<div class=\"cool\">Hello</div>", $view);

// Creating custom Elements
$view = Element::create('p')
  ->addText("Hello world.");   
````  
## Controller
@TODO: After we have built the base controller classes

