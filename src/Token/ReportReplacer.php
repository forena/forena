<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:59 AM
 */

namespace Forena\Token;


use Forena\Data\DataService;

class ReportReplacer {

  protected $tpattern =  '/\{[^\n^\r^}]+}/';
  protected $trim_chars = '{}';

  /** @var DataService */
  protected $data;

  /**
   * ReportReplacer constructor.
   */
  public function __construct() {
    $this->data = DataService::service();
  }

  /**
   * Return a formatted value of a string.
   *
   * @param string $expression
   *   The property anme or expression to evaluate.
   * @param bool $raw
   *   Indicates whether special formatting should be
   *   applied to the data.
   */
  public function getValue($expression, $raw=TRUE) {
    $value = DataService::service()->getValue($expression);
    return $value;
  }

  /**
   * Replaces tokens in a text string.
   * @param $text
   *   Text to peform token replacement on.
   * @param bool $raw
   *   True implies that data will not be formatted.
   * @return string
   */
  public function replace($text, $raw=TRUE) {
    if (is_array($text)) {
      foreach ($text as $key => $value) {
        $text[$key] = $this->replace($value);
      }
      return $text;
    }
    //Otherswise assume text
    $match=array();
    $o_text = $text;

    // Put the data on the stack.
    if (preg_match_all($this->tpattern, $o_text, $match)) {
      // If we are replacing a single value then return exactly
      // the single value in its native type;
      $single_value = ($match && count($match[0]) == 1 && $match[0][0]==$text);

      foreach ($match[0] as $match_num => $token) {
        $expression = trim($token, $this->trim_chars);
        $value = $this->getValue($expression, $raw);
        if ($single_value) {
          return $value;
        }
        else {
          $pos = strpos($text, $token);
          if ($pos !== FALSE) {
            $text = substr_replace($text, $value, $pos, strlen($token));
          }
        }
      }
    }
    return $text;
  }

  /**
   * Replaces nested array data.
   * @param $data
   *   The array containing values to replace.
   * @param $raw
   *   TRUE implies no formatting of data.
   */
  public function replaceNested(&$data, $raw = TRUE) {
    if (is_array($data)) {
      $values = $data;
      foreach ($values as $k => $value) {
        // Replace key data
        $key = $k;
        if (strpos($k, '{') !== FALSE) {
          $key = $this->replace($key);
          unset($data[$k]);
          $data[$key] = $value;
        }

        // Replace value data.
        if (is_array($value)) {
          $this->replaceNested($data[$key], $raw);
        }
        else {
          $data[$key] = $this->replace($value, $raw);
        }
      }
    }
    else {
      $data = $this->replace($data, $raw);
    }
  }


}