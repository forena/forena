<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 5:55 PM
 */

namespace Forena\Data;


/**
 * Class DataService
 *
 * Data contexts are used for token replacements and passing "model" data to
 * both views and controllers.  Applications push data onto a stack of data
 * contexts.  These contexts may be retrieved for iteration or token replacement
 * or controller decisioins.
 *
 * @package Forena\Data
 */
class DataService {
  static protected $service;
  private $id;
  private $contexts=[];
  private $cur_context;  // The data of the xml;
  private $cur_context_xml; //
  private $context_stack = array();
  private $id_stack = array();

  /**
   * Return the current data context
   *
   */
  public function currentContext() {
    return $this->cur_context;
  }


  /**
   * @param $id
   * @return ContextInterface|null
   */
  public function getContext($id) {
    if (isset($this->contexts[$id])) {
      return $this->contexts[$id];
    }
    else {
      return NULL;
    }
  }

  /**
   * Retrieves a value from a current context.
   * @param $expression
   * @return mixed|null|\SimpleXMLElement|string
   */
  public function getValue($expression) {
    $value = NULL;
    if (strpos($expression, '.')) {
      list($id, $property) = explode('.', $expression);
      if (isset($this->contexts[$id])) {
        $value = $this->getValueFromContext($property, $this->contexts[$id]);
      }
    }
    else {
      $value = $this->getValueFromContext($expression);
    }

    return $value;
  }


  /**
   * @param string $key
   *   The key of the property in the context to be retrieved.
   * @parma string $context
   *   The id of the context to retrieve the property from
   *   current context is used when not specified.
   * @return string
   */
  protected function getValueFromContext($key, $context = NULL) {
    $retvar = '';
    // Default to theo current context
    if (!$context) $context = $this->currentContext();

    // If it is a simple variable expression just get the data.
    if (!preg_match('/[\=\@\[\/\]\(\)]/', $key)) {
      if (is_array($context)) {
        $retvar = @$context[$key];
      }
      elseif (is_object($context)
        && (isset($context->$key) || is_callable([$context, '__get']) )
        ) {
        $retvar = $context->$key;
      }
    }
    // Looks like we have an xpath expression so we need to run xpath query
    // get the data
    elseif (is_object($context) || is_array($context)) {
      if (is_array($context) ) {
        if ( !$this->cur_context_xml) $this->cur_context_xml = DataService::arrayToXml($context);
        $context = $this->cur_context_xml;
      }
      elseif (!method_exists($context, 'xpath')) {
        if(method_exists($context, 'asXML')) {
          $xml = $context->asXML();
          if (!is_object($xml) && $xml) $xml = new \SimpleXMLElement($xml);
          $this->cur_context_xml = $xml;
        }
      }

      if (strpos($key, '=')===0) {
        $retvar = $this->simplexml_evaluate($context, ltrim($key, '='));
      }
      else {
        $x ='';
        if (isset($context->$key)) {
          $x = $context->$key;
        }
        elseif (method_exists($context, 'xpath')) {
          $rows = @$context->xpath($key);
          if ($rows) $x = $rows[0];
        }

        if ($x && is_object($x) && method_exists($x, 'asXML')) {
          $retvar = $x->asXML();
          // Check to see if there are child nodes
          // If so use asXML otherwise string cast.
          if ($retvar && strpos($retvar, '<')!==FALSE) {
            // Find the end of the first tag.
            $p = strpos($retvar, '>');
            $retvar = substr_replace($retvar, '', 0, $p+1);
            $p = strrpos($retvar, '<', -1);
            $retvar = substr_replace($retvar, '', $p, strlen($retvar) - $p);

          }
          else {
            $retvar = (string)$x;
          }
        }
        else {
          $retvar = &$x;
        }
      }
    }

    if (!is_array($retvar)) {
      if (is_object($retvar) && is_a($retvar, 'DOMNodeList')) {
        $retvar = $retvar->item(0);
        if ($retvar) ($retvar = trim($retvar->textContent));
      }
      else {
        $retvar = trim((string)$retvar);
      }
    }
    return $retvar;
  }


  /**
   * Convert a data context to an array.
   *
   * @parm mixed $context
   *   The data to be converted or cast as an array.
   */
  static public function toArray($context) {
    if (is_array($context)) {
      return $context;
    }

    if (is_object($context)) {
      // Get attributes
      $ret = get_object_vars($context);
      if (method_exists($context, 'attributes')) {
        foreach ($context->attributes() as $key => $value) {
          $ret[$key] = (string)$value;
        }
      }
    }
    else {
      $ret = (array)$context;
    }
    return $ret;
  }

  /**
   * Provides an api to the {=xpath} syntax that can be used
   * to evaluate expressions such as sum and count in a report.  We
   * need to use the DOM object here, because this method isn't exposed
   * with simplexml.
   *
   * @param \SimpleXMLElement $xml
   * @param string $path
   * @return \SimpleXMLElement
   */
  protected function simplexml_evaluate($xml, $path) {
    if (!method_exists($xml, 'xpath')) return NULL;
    $dom_node = dom_import_simplexml($xml);
    $dom_doc = new \DOMDocument('');
    $dom_node = $dom_doc->importNode($dom_node, TRUE);
    $dom_doc->appendChild($dom_node);
    // Do we also need to call AppendChild?
    $xpath = new \DOMXpath($dom_doc);
    $ret = $xpath->evaluate($path, $dom_node);
    return $ret;
  }


  /**
   * Convert an array to XML.
   * @param array $a
   *   Array to be converted.
   * @param \SimpleXMLElement $xml
   *   The XML node o which to add the array.  If none is spacified a new
   *   <root/> element is manufactured.
   * @return null|\SimpleXMLElement
   */
  static public function arrayToXml(array $a, \SimpleXMLElement $xml=NULL) {
    if (!$xml) $xml = new \SimpleXMLElement('<root/>');
    $tag = '';
    foreach($a as $k => $v) {
      if (preg_match('/^[0-9\-\.]/', $k)) {
        if (!$tag) $tag = "element";
      }
      else {
        $tag = $k;
      }
      if (is_array($v) || is_object($v)) {
        $node = $xml->addChild($tag, '');
        if ($tag != $k) {
          $node['key'] = $k;
        }
        $node['type'] = is_object($v) ? 'object' : 'array';
        DataService::arrayToXml($v, $node);
      }
      else {
        $node = $xml->addChild($tag, htmlspecialchars($v));
        $node['key'] = $k;
      }

      $tag =  preg_replace( '/[^a-zA-Z0-9]/', '_', (string) $k);
    }
    return $xml;
  }


  /**
   * Push a data context onto the data stacks
   * to make sure that we can address these using an
   * appropriate syntax.  I think we don't need data_stack
   * but i'm holding it there in case we develop a "relative" path syntax.
   * @param $context
   * @param $id
   */
  public function addContext(&$context, $id='') {
    $this->context_stack[] = $this->cur_context;
    $this->id_stack[] = $this->id;
    $this->id = $id;
    $this->cur_context = $context;
    $this->cur_context_xml = '';

    if ($id) {
      /*    if (@is_array($this->data_sources[$id]) && is_array($data)) {
       $data = array_merge($this->data_sources[$id], $data);
       }
       */
      $this->contexts[$id] = $context;
    }
  }

  /**
   * @param $id
   *   Id of the data context to be set
   * @param $context
   *   Data of the data context to be set.
   */
  public function setContext($id, &$context) {
    if (is_object($context)) {
      $this->contexts[$id] = $context;
    }
    else {
      $this->contexts[$id] = &$context;
    }
  }

  /**
   * Remove data from the data stack.
   *
   * This will make data unavaiable when we leave the context of the current
   * nested reports.
   */
  public function pop() {
    $this->id = array_pop($this->id_stack);
    $this->cur_context = array_pop($this->context_stack);
    $this->cur_context_xml = '';
  }

  /**
   * Determines whether an array context exists for the specified id.
   * Returns true if the key exists othewise false
   * @param string $id
   * @return bool
   */
  public function contextExists($id) {

    $exists = FALSE;
    if (array_key_exists($id, $this->contexts)) {
      $exists = TRUE;
    }
    return $exists;
  }

  public function selectContext($id) {
    if (isset($this->contexts[$id])) {
      $this->cur_context = &$this->contexts[$id];
    }
  }


  /**
   * Singleton factory.
   * @return static
   */
  static public function service() {
    if (static::$service == NULL) {
      static::$service = new static();
    }
    return static::$service;
  }
}
