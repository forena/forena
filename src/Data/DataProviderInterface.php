<?php
/**
 * Created by PhpStorm.
 * User: David.Metzler
 * Date: 5/25/2018
 * Time: 5:25 PM
 */

namespace Forena\Data;


interface DataProviderInterface
{

  /**
   * Determine whether the user has access to the data.
   *
   * @param $path
   * @return mixed
   */
  public function access($path);

  /**
   * Retrieve data from a relative path.
   *
   * @param $path
   * @return mixed
   */
  public function getData($path);
}