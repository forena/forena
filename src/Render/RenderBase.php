<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 8:50 AM
 */

namespace Forena\Render;


use Forena\View\ViewBase;
use Forena\View\ViewInterface;

abstract class RenderBase implements RenderInterface {

  /** @var bool Indicates whether element has been rendered */
  private $is_rendered=FALSE;

  /** @var  ViewInterface */
  public $view;

  /**
   * Adds data to the current view.
   * @param $buffer
   */
  protected function addToView($buffer) {
    $this->ensureView();
    $this->view->add($buffer);
  }

  /**
   * Make sure that we actually have generated a view.
   */
  protected function ensureView() {
    if (!$this->view) {
      $this->view = ViewBase::generateDefaultView();
    }
  }



  /**
   * Render and display the element.
   * @return mixed
   */
  public function show() {
    if (!$this->is_rendered) {
      $this->render();
    }
    return $this->view->show();
  }

  /**
   * Render the current element on the view.
   * @return $this
   */
  public function render() {
    $this->ensureView();
    $this->is_rendered = TRUE;
    return $this;
  }


}