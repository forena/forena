<?php


namespace Forena\Render\HTML;

use Forena\Data\DataService;
use Forena\Render\RenderBase;
use Forena\Render\RenderInterface;

/**
 * Class Element
 *
 * Contains an exmaple
 * @package Forena\Render\HTML
 */
class Element extends RenderBase {

  /* Override this constant in derived classes */
  const TAG_NAME = 'html';

  /** List of self closing tags in HTML */
  const SELF_CLOSING_TAGS = [
    'area',
    'base',
    'br',
    'col',
    'comamnd',
    'embed',
    'hr',
    'img',
    'input',
    'keygen',
    'link',
    'meta',
    'param',
    'source',
    'track',
    'wbr',
    ];

  /** @var string name of HTML tag to render */
  public $tag_name;

  /** @var array Attributes of HTML tag */
  protected $attributes = [];

  /** @var bool Indicates  */
  protected $is_self_closing = FALSE;

  /** @var  object|array */
  private $repeat_data;

  /** @var RenderInterface */
  protected $contents=[];

  public function __construct(array $attributes=[]) {
    $this->setTagName(static::TAG_NAME);
    $this->setAttributes($attributes);
  }

  /**
   * @param $tag_name
   * @return $this
   */
  public function setTagName($tag_name) {
    // @TODO: Sanitize Tag names
    $this->tag_name = $tag_name;
    if (array_search($tag_name, static::SELF_CLOSING_TAGS)) {
      $this->is_self_closing = TRUE;
    }
    return $this;
  }

  /**
   * Set an HTML Attribute.
   * @param $key
   * @param $value
   */
  public function setAttribute($key, $value) {
    $this->attributes[$key] = $value;
  }

  /**
   * Set html attributes on the object.
   * @param array $attributes
   */
  public function setAttributes(array $attributes) {
    foreach ($attributes as $key => $value) {
      $this->setAttribute($key, $value);
    }
  }

  /**
   * Set HTML5 data- attributes based on other key/value pairs.
   * @param array $attributes
   */
  public function setDataAttributes(array $attributes) {
    foreach($attributes as $key => $value ) {
      $this->setAttribute("data-$key", $value);
    }
  }

  /**
   * Generates the Attribute string fof including on elmeents.
   */
  protected function getHTMLEncodedAttributes() {
    $attrs = '';
    $this->ensureView();
    foreach ($this->attributes as $key => $value) {
      $value = htmlentities($this->view->replace($value));
      $attrs .= "$key=\"$value\" ";
    }
    if ($attrs) $attrs = " ". rtrim($attrs);
    return $attrs;
  }

  /**
   * Add a child element to the contents of the container.
   *
   * @param \Forena\Render\HTML\Element $element
   * @return $this
   */
  public function addElement(Element $element) {
    $element->view = $this->view;
    $this->contents[] = $element;
    return $this;
  }

  /**
   * Add text to a container.
   * @param string $text
   *   encoded html text to add
   * @return $this
   */
  public function addText($text) {
    $text = new Text($text);
    $text->view = $this->view;
    $this->contents[] = $text;
    return $this;
  }

  /**
   * Renders an individual element.
   */
  protected function renderElement() {
    $attrs = $this->getHTMLEncodedAttributes();
    if ($this->is_self_closing) {
      $this->addToView("<" . $this->tag_name . $attrs . "/>");
    }
    else {
      $this->addToView("<" . $this->tag_name . $attrs . ">");
      $this->renderContents();
      $this->addToView("</" . $this->tag_name . ">");
    }
  }

  /**
   * Renders the contents of the interior of the element.
   * @return $this
   */
  public function renderContents() {
    /** @var RenderInterface $content */
    foreach ($this->contents as $content) {
      $content->render();
    }
    return $this;
  }

  /**
   * Render the corresponding element.
   * @return $this
   */
  public function render() {
    if ($this->repeat_data) {
      foreach ($this->repeat_data as $data) {
        $ds = DataService::service();
        $ds->addContext($data);
        $this->renderElement();
        $ds->pop();
      }
    }
    else {
      $this->renderElement();
    }
    // Determine if we need a repeating element
    parent::render();
    return $this;
  }


  /**
   * Factory method for syntactical sugar.
   *
   * @param array $attributes
   * @return static
   */
  public static function tag($attributes = []) {
    return new static($attributes);
  }

  /**
   * Manufacture a new tag.
   * @param $tag
   * @param array $attributes
   * @return Element
   */
  public static function create($tag, $attributes = []) {
    return Element::tag($attributes)
      ->setTagName($tag);
  }

  /**
   * @param mixed $data
   * @return $this
   */
  public function repeatOver($data) {
    $this->repeat_data = $data;
    return $this;
  }
}
