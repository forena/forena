<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 9:18 AM
 */

namespace Forena\Render\HTML;


use Forena\Render\RenderBase;

class Text extends RenderBase {
  public $contents = '';

  /**
   * Text constructor.
   * @param $text
   */
  public function __construct($text) {
    $this->contents = $text;
  }

  public function render() {
    parent::render();
    $this->addToView(htmlspecialchars($this->view->replace($this->contents)));
    return $this;
  }

}