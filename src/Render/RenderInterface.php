<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/20/17
 * Time: 9:28 PM
 */

namespace Forena\Render;

interface RenderInterface {
  /**
   * Renders elements.
   * @return RenderInterface
   */
  public function render();

  /**
   * Return the rendered result in an appropriate manner.
   */
  public function show();

}