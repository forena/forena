<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/19/2017
 * Time: 11:57 AM
 */

namespace Tas\Core\Renderer\SVG;


use Forena\Render\HTML\Element;

class Rectangle extends Element {
  const TAG_NAME = 'rect';
  public $x=0;
  public $y=0;
  public $width=0;
  public $height=0;

  public function __construct($x, $y , $width, $height, array $attributes = []) {
    $this->x = $x;
    $this->y = $y;
    $this->width = $width;
    $this->height = $height;
    parent::__construct($attributes);
  }

  public function render() {
    $this->attributes['x'] = (int)$this->x;
    $this->attributes['y'] = (int)$this->y;
    $this->attributes['width'] = (int)$this->width;
    $this->attributes['height'] = (int)$this->height;
    return parent::render();
  }
}