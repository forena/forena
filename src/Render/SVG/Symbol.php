<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/19/2017
 * Time: 11:03 AM
 */

namespace Tas\Core\Renderer\SVG;


use Forena\Render\HTML\Element;

class Symbol extends Element {
  const TAG_NAME = 'symbol';

  public $id;

  /**
   * Symbol constructor.
   * @param string $id
   * @param array $attributes
   */
  public function __construct($id, array $attributes = []) {
    parent::__construct($attributes);
    $this->setAttribute('id', $id);
    $this->id=$id;
  }
}