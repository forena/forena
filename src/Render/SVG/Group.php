<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/19/2017
 * Time: 11:58 AM
 */

namespace Tas\Core\Renderer\SVG;


use Forena\Render\HTML\Element;


class Group extends Element {
  const TAG_NAME = 'g';
}