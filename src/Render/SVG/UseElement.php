<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/19/2017
 * Time: 2:54 PM
 */

namespace Tas\Core\Renderer\SVG;


use Forena\Render\HTML\Element;


class UseElement extends Element {
  const TAG_NAME = 'use';

  protected $id;

  /**
   * UseElement constructor.
   * @param string $id
   * @param array $attributes
   */
  public function __construct($id, array $attributes = []) {
    parent::__construct($attributes);
    $this->id = $id;
  }

  public function render() {
    $this->attributes['xlink:href'] = '#'. $this->id;
    parent::render();
  }
}