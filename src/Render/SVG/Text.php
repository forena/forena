<?php

namespace Tas\Core\Renderer\SVG;

use Forena\Render\HTML\Element;

class Text extends Element {
  const TAG_NAME = 'text';

  public $text;

  public function __construct(array $attributes) {
    parent::__construct($attributes);
  }

  public function render() {
    $this->contents = htmlentities($this->text);
    parent::render();
  }


}