<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/19/2017
 * Time: 9:03 AM
 */

namespace Tas\Core\Renderer\SVG;

use Forena\Render\HTML\Element;

class SVG extends Element {
  const TAG_NAME = 'svg';
  public $box_height = 1000;
  public $box_width = 1000;

  /** @var Element[] */
  public $definitions;

  /**
   * SVG constructor.
   * @param int $box_height
   *   Height of viewbox
   * @param int $box_width
   *   Width of viewbox.
   * @param array $attributes
   */
  public function __construct($box_height, $box_width, array $attributes = []) {
    parent::__construct($attributes);
    $this->setAttribute('xmlns:xlink' , "http://www.w3.org/1999/xlink");
    $this->box_height = $box_height;
    $this->box_width = $box_width;
    $this->definitions = new Element();
    $this->definitions->tag_name = 'def';
  }

  public function renderContents() {
    if ($this->definitions->contents) {
      $this->definitions->render();
    };
    parent::renderContents();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $this->setAttribute('viewBox', [0, 0, $this->box_width, $this->box_height]);
    parent::render();
  }

}