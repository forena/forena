<?php

namespace Forena;

use Forena\Data\DataProviderInterface;
use Forena\Template\FrxTemplate;
use Forena\Template\HTMLTemplate;

/**
 * Forena API service class
 */
class Forena
{
  /**
   * @var Forena
   */
  static private $instance;

  /**
   * @var  string[]
   *   Array of paths to search for templates.
   */
  protected $template_directories = [];

  /**
   * @var DataProviderInterface[]
   */
  protected $data_providers = [];

  /**
   * Singleton factory
   * @return Forena
   */
  static public function service() {
    if (static::$instance === NULL) {
      static::$instance = new Forena();
    }
    return static::$instance;
  }

  /**
   * Register a directory containing forena templates.
   * @param string $path path to templates.
   */
  public function registerTemplates($path) {
    if (array_search($path, $this->template_directories) === FALSE) {
      if (is_dir($path)) {
        $this->template_directories[] = $path;
      }
    }
  }

  protected function findTemplateFile($file_name) {
    foreach($this->template_directories as $template_directory) {
      if (file_exists($template_directory . '/'. $file_name)) {
        return $template_directory . '/' . $file_name;
      }
    }
    return NULL;
  }

  /**
   * Loads an HTML5 Forena template file.
   *
   * @param $file_name
   * @return Render\HTML\Element|null
   */
  public function loadTemplate($file_name) {
    $element = NULL;
    $template = $this->findTemplateFile($file_name);
    if ($template) {
      $element = HTMLTemplate::load($template);
    }
    return $element;
  }

  /**
   * Loads an Frx report template from a file.
   * @param $file_name
   * @return Render\HTML\Element|null
   */
  public function loadReport($file_name) {
    $element = NULL;
    $template = $this->findTemplateFile($file_name);
    if ($template) {
      $element = FrxTemplate::load($template);
    }
    return $element;
  }

  /**
   * Register a data provider for subsequent calls data.
   * @param $name
   * @param DataProviderInterface $provider
   */
  public function registerDataProvider($name, DataProviderInterface $provider) {
    $this->data_providers[$name] = $provider;
  }

  /**
   * Use registered providers to retrieve data from a path.
   *
   * @param string $provider
   *   Name of registered provider
   * @param $path
   *   Path on provider from with to access data.
   * @return mixed|null
   */
  public function getData($provider, $path) {
    $data = NULL;
    if (!empty($this->data_providers[$provider])) {
      $data_provider = $this->data_providers[$provider];
      if ($data_provider->access($path)) {
        $data = $data_provider->getData($path);
      }
    }
    return $data;
  }


}