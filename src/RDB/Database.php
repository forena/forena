<?php


namespace Forena\RDB;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;

/**
 * Class Database
 *
 * Provides core Relational Database via doctrine/dbal composer package.
 */
class Database {

  CONST DEFAULT_DB_CONFIG = [];

  // The default environment variable prefix
  CONST DEFAULT_ENVIRONMENT_PREFIX = '';

  protected $environment_prefix;

  /**
   * @var Connection
   *  Current connection to database.
   */
  protected $db;

  /**
   * @var EventManager
   */
  protected $eventManager;

  /**
   * @var array
   *   Configuration array
   */
  protected $configuration;

  /**
   * @var int
   *   Nesting level of transaction
   */
  protected $transactions;

  /**
   * Database constructor.
   */
  public function __construct() {
    $this->configuration = static::DEFAULT_DB_CONFIG;
    $this->environment_prefix = static::DEFAULT_ENVIRONMENT_PREFIX;
    $this->eventManager = new EventManager();
  }


  /**
   * Set the connection to an existing DBAL connection.
   * @param Connection $db
   */
  public function setConnection(Connection $db) {
    $this->db = $db;
  }

  /**
   * Configure a database.
   * @param array $configuration
   */
  public function configure(array $configuration) {
    $this->configuration = array_merge($this->configuration, $configuration);
  }

  /**
   * Alters the configuration based on environment variables
   */
  protected function alterConfiguration() {
    $environment = array_merge($_ENV, $_SERVER);
    foreach ($environment as $key => $value) {
      if (strpos($key, $this->environment_prefix)===0) {
        $new_key = strtolower(substr($key, strlen($this->environment_prefix)));
        $this->configuration[$new_key] = $value;
      }
    }
  }

  /**
   * Returns the current doctrine DBAL connection
   * @throws DBALException
   * @return Connection
   */
  public function getConnection() {
    if (!$this->db) {
      $config = new Configuration();
      if ($this->environment_prefix) {
        $this->alterConfiguration();
      }
      $this->db =  DriverManager::getConnection($this->configuration, $config, $this->eventManager);
    }
    return $this->db;
  }

  /**
   * Begin a transaction
   */
  public function beginTransaction() {
    if ($this->db) {
      $this->transactions++;
      $this->db->beginTransaction();
    }
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  public function rollback() {
    if ($this->db && $this->transactions) {
      $this->transactions = 0;
      $this->db->rollBack();
    }
  }

  /**
   * End a transaction
   * @throws \Doctrine\DBAL\ConnectionException
   */
  public function endTransaction() {
    if ($this->transactions) {
      $this->transactions--;
      if ($this->db) {
        $this->db->commit();
      }
    }
  }
}
