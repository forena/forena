<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/13/2017
 * Time: 9:25 AM
 */

namespace Forena\Template;


use Forena\Render\HTML\Div;
use Forena\Render\HTML\Element;

/**
 * Class TemplateBase
 *
 * Implements a base class for common fucntions for XHTML DOM based templates.
 * @package Forena\Template
 */
abstract class TemplateBase implements TemplateInterface {

  /** @var  Element */
  protected $element;

  /** @var  \DOMDocument */
  protected $dom;

  /** @var  Element */
  protected $cur_element;

  /**
   * TemplateBase constructor.
   * @param $file_name
   */
  public function __construct($file_name, Element $element=NULL) {
    $this->dom = new \DOMDocument('1.0', 'UTF-8');
    if ($element) {
      $this->element = $element;
    }
    else {
      $this->element = Div::tag();
    }

    if ($file_name) {
      $text = file_get_contents($file_name);
      $this->parse($text);
    }
  }

  /**
   * Preprocess a normal element, prior to processing its children.
   * @param \SimpleXMLElement $node
   * @return mixed
   */
  protected abstract function preprocessElement(\SimpleXMLElement $node);

  /**
   * Process a dom element from the template.
   * @param \DOMNode $node
   */
  protected function processDomNode(\DOMNode $node) {
    switch($node->nodeType) {
      case XML_TEXT_NODE:
      case XML_ENTITY_NODE;
      case XML_ENTITY_REF_NODE:
        $this->cur_element->addText($node->textContent);
        break;
      case XML_COMMENT_NODE:
        // @TODO: Process comment node
        break;
      default:
        // Continue processing non text nodes
        $sn = simplexml_import_dom($node);
        // Special catch to make sure we don't process bad nodes
        if (is_object($sn)) {
          $cur_element = $this->cur_element;
          $continue = $this->preprocessElement($sn);
          if ($continue) {
            foreach($node->childNodes as $child_node) {
              $this->processDomNode($child_node);
            }
          }
          $this->cur_element = $cur_element;
        }
    }
  }

  /**
   * @return \DOMNode
   */
  protected abstract function getStartNode();

  /**
   * @param string $text
   *   Template text to pares.
   */
  public function parse($text) {
    // Load document and simplexml representation
    try {
      $this->dom->loadXML($text);
    }
    catch(\Exception $e) {
      return NULL;
    }

    $this->cur_element = $this->element;
    // Extract the basic element.
    $container = $this->getStartNode();
    foreach ($container->childNodes as $node) {
      $this->processDomNode($node);
    }
  }

  /**
   * @return Element
   */
  public function getRenderElement() {
    return $this->element;
  }

  /**
   * Creates a tree of elements from a template file.
   * @param $file_name
   * @return \Forena\Render\HTML\Element
   */
  public static function load($file_name, Element $element=NULL) {
    $template = new static($file_name, $element);
    return $template->getRenderElement();
  }
}
