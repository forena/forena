<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/13/2017
 * Time: 9:24 AM
 */

namespace Forena\Template;


use Forena\Data\DataService;
use Forena\Render\HTML\Element;

class HTMLTemplate extends TemplateBase {
  /**
   * @return \DOMElement
   */
  protected function getStartNode() {
    return $this->dom->documentElement;
  }

  protected function preProcessElement(\SimpleXMLElement $node) {
    $continue = TRUE;
    $tag_name = $node->getName();
    $attributes = [];

    if (strpos($tag_name, '-')) {
      list($tag_name,$action) = explode('-', $tag_name);
    }
    else {
      $action='';
    }


    // Load the basic Html attributes
    foreach($node->attributes() as $key => $attribute) {
      $attributes[$key] = (string)$attribute;
    }

    // Create a new render element
    $element = Element::create($tag_name, $attributes);

    // Process the prior collected action
    switch ($action) {
      case 'foreach':
        $context = DataService::service()->currentContext();
        $element->repeatOver($context);
        break;
    }
    $this->cur_element->addElement($element);
    $this->cur_element = $element;
    return $continue;
  }
}