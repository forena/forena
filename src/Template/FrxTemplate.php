<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/8/17
 * Time: 1:25 PM
 */

namespace Forena\Template;


use Forena\Data\DataService;
use Forena\Render\HTML\Element;

class FrxTemplate extends TemplateBase {

  CONST FRXNS='urn:FrxReports';

  /** @var string  */
  protected $frxns;

  /** @var  array FRX Namespaced attributes. */
  protected $frx_attributes;

  public function __construct($file_name) {
    $this->frxns = static::FRXNS;
    parent::__construct($file_name);
  }

  /**
   * @return \DOMElement
   */
  protected function getStartNode() {
    return $this->dom->getElementsByTagName('body')->item(0);
  }

  /**
   * Process a template node.
   * @param \SimpleXMLElement $node
   */
  protected function preprocessElement(\SimpleXMLElement $node) {
    $continue = TRUE;
    $tag_name = $node->getName();
    $attributes = [];
    $this->frx_attributes=[];

    // Load the basic Html attributes
    foreach($node->attributes() as $key => $attribute) {
      $attributes[$key] = (string)$attribute;
    }

    // Create a new render element
    $element = Element::create($tag_name, $attributes);

    //Load all the the FRX Attributes
    foreach($node->attributes($this->frxns) as $key=>$attribute) {
      $this->frx_attributes[strtolower($key)] = (string)$attribute;
    }

    // Now process them this is done in two passes to make sure that we
    // implement a frx command that needs data from another one.
    foreach($this->frx_attributes as $key => $value) {
      switch ($key) {
        case "foreach":
          $context = DataService::service()->currentContext();
          $element->repeatOver($context);
          break;
      }
    }


    $this->cur_element->addElement($element);
    $this->cur_element = $element;
    return $continue;
  }




}