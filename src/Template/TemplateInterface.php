<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/13/2017
 * Time: 9:23 AM
 */

namespace Forena\Template;

/**
 * Interface TemplateInterface
 *
 * Defines how the fornea system will interact with templates.
 * @package Forena\Template
 */
interface TemplateInterface {

  /**
   * Creates a tree of elements from a template file.
   * @param $file_name
   * @return \Forena\Render\HTML\Element
   */
  public static function load($file_name);
}
