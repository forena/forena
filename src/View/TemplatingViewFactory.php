<?php

namespace Forena\View;

use Forena\Render\HTML\Element;
use Forena\Template\FrxTemplate;
use Forena\Template\HTMLTemplate;

/**
 * Class TemplatingViewFactory
 *
 * Used to manufacture views from templates.
 * @package Forena\View
 */
class TemplatingViewFactory {

  protected $template_dir;

  /**
   * @var static
   */
  static private $instance;

  public function __construct($dir) {
    $this->template_dir = rtrim($dir, '\/');
  }

  /**
   * @param $dir
   * @return static
   */
  static public function create($dir) {
    static::$instance = new static($dir);
    return static::$instance;
  }

  /**
   * @return static
   */
  static public function service() {
    return static::$instance;
  }

  /**
   * @param string $file
   *   Name of template file to load
   * @param string $tag
   *   Tag to use as containing element
   * @param array $attributes
   *   HTML attributes to use on the tag.
   */
  public function HTMLTemplate($file, $tag='div', $attributes=[]) {
    $element = Element::create($tag, $attributes);
    $template = HTMLTemplate::load($this->template_dir . "/". $file, $element);
    return $template;
  }

  public function FrxTemplate($file, $tag='div', $attributes=[]) {
    $element = Element::create($tag, $attributes);
    $template = FrxTemplate::load($this->template_dir . "/". $file, $element);
    return $template;
  }

}