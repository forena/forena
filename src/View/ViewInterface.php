<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 10:48 AM
 */

namespace Forena\View;


interface ViewInterface {

  /**
   * Adds data to the view.
   * @param string $buffer
   */

  public function add($buffer);

  /**
   * Display the final view.
   */
  public function show();

  /**
   * Provide toeken replacement for the view.
   * @return string
   */
  public function replace($text);

}