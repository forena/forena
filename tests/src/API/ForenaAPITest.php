<?php

namespace Forena\Tests\API;

use Forena\Forena;
use Forena\Render\HTML\Element;
use Forena\Tests\ForenaUnitTestCase;
use Forena\Tests\Mock\MockDataProvider;
use Forena\Tests\Mock\TestClassSimpleClass;

/**
 * Class ForenaAPITest
 * @coversDefaultClass Forena
 */
class ForenaAPITest extends ForenaUnitTestCase
{
  /** @var  Forena */
  protected $forena;

  public function setUp() {
    parent::setUp();
    $this->forena = Forena::service();
  }

  /**
   * @covers Forena::service()
   */
  public function testService() {
    $this->assertInstanceOf(Forena::class, $this->forena);
  }

  /**
   * @covers Forena::registerTemplates()
   * @covers Forena::loadReport()
   * @covers Forena::loadTemplate()
   */
  public function testRegisterTemplates() {
    $this->forena->registerTemplates($this->test_dir . '/templates');
    $view = $this->forena->loadTemplate('test.html');
    $this->assertInstanceOf(Element::class, $view);
    $view = $this->forena->loadReport('test.frx');
    $this->assertInstanceOf(Element::class, $view);
  }

  /**
   * @covers Forena::registerDataProvider()
   * @covers Forena::getData()
   */
  public function testRegisterDataProvider() {
    $data_provider = new MockDataProvider();
    $this->forena->registerDataProvider('test', $data_provider);
    $data = $this->forena->getData('test', 'data');
    $this->assertInstanceOf(TestClassSimpleClass::class, $data);
    $data_provider->access = FALSE;
    $data = $this->forena->getData('test', 'data');
    $this->assertEmpty($data);
  }
}