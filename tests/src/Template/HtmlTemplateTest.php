<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/13/2017
 * Time: 9:20 AM
 */

namespace Forena\Tests\Template;


use Forena\Data\DataService;
use Forena\Render\HTML\Div;
use Forena\Render\HTML\Element;
use Forena\Template\HTMLTemplate;
use Forena\Tests\ForenaUnitTestCase;
use Forena\Tests\Mock\TestClassSimpleClass;

class HtmlTemplateTest extends ForenaUnitTestCase {
  /**
   * Verify that simple token replacement works.
   */
  public function testHtmlTemplate() {
    $data = new TestClassSimpleClass();
    DataService::service()->addContext($data, 'test');
    $template = HTMLTemplate::load(__DIR__ . '/templates/html_template.html');


    // Make sure that we have a render element as a class.
    $this->assertInstanceOf(Element::class, $template);

    $html = $template->show();
    $this->assertContains('<p>Hello World!</p>', $html);
  }

  /**
   * Verify that simple iteration works.
   */
  public function testForEach() {
    $data = new TestClassSimpleClass();
    DataService::service()->setContext('test', $data);
    DataService::service()->addContext($data->colors, 'colors');

    $template = HTMLTemplate::load(__DIR__ . '/templates/html_template.html');
    $html = $template->show();

    $this->assertContains("<li>red</li>", $html);
    $this->assertContains("<li>yellow</li>", $html);
    $this->assertContains("<li>blue</li>", $html);
  }

  public function testAppendToElement() {
    $data = new TestClassSimpleClass();
    DataService::service()->addContext($data, 'test');
    $div = Div::tag(['class' => 'cool']);
    $template = HTMLTemplate::load(__DIR__ . '/templates/html_template.html', $div);

    // Make sure that we have a render element as a class.
    $this->assertInstanceOf(Element::class, $template);

    $html = $div->show();
    $this->assertContains('<p>Hello World!</p>', $html);
  }

}