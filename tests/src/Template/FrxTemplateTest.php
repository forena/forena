<?php

namespace Forena\Tests\Template;

use Forena\Data\DataService;
use Forena\Render\HTML\Element;
use Forena\Template\FrxTemplate;
use Forena\Tests\Mock\TestClassSimpleClass;

/**
 * Frx Templating Engine tests
 */
class FrxTemplateTest extends \Forena\Tests\ForenaUnitTestCase {

  /**
   * Verify that simple token replacement works. 
   */
  public function testSimpleTemplate() {
    $data = new TestClassSimpleClass();
    DataService::service()->addContext($data, 'test');
    $template = FrxTemplate::load(__DIR__ . '/templates/simple_template.frx');


    // Make sure that we have a render element as a class.
    $this->assertInstanceOf(Element::class, $template);

    $html = $template->show();
    $this->assertContains('<p>Hello World!</p>', $html);
  }

  /**
   * Verify that simple iteration works.
   */
  public function testForEach() {
    $data = new TestClassSimpleClass();
    DataService::service()->setContext('test', $data);
    DataService::service()->addContext($data->colors, 'colors');

    $template = FrxTemplate::load(__DIR__ . '/templates/simple_template.frx');
    $html = $template->show();

    $this->assertContains("<li>red</li>", $html);
    $this->assertContains("<li>yellow</li>", $html);
    $this->assertContains("<li>blue</li>", $html);

  }
}