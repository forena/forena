<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 8:35 AM
 */

namespace Forena\Tests;


use PHPUnit\Framework\TestCase;

/**
 * @covers
 */
abstract class ForenaUnitTestCase extends TestCase {
  protected $test_dir;

  public function setUp() {
    parent::setUp();
    $this->test_dir = dirname(__DIR__);
  }
}