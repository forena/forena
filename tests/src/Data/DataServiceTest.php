<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 5:45 PM
 */

namespace Forena\Data;

use Forena\Tests\ForenaUnitTestCase;
use Forena\Tests\Mock\TestClassSimpleClass;


/**
 * Class DataServiceTest
 * @package Forena\Data
 * @coversDefaultClass DataService
 */
class DataServiceTest extends ForenaUnitTestCase {

  /** @var DataService */
  protected $service;

  public function setUp() {
    parent::setUp();
    $this->service = new DataService();
  }

  /**
   * @covers DataService::getContext()
   * @covers DataService::addContext()
   */
  public function testContext() {

    // Put the data on the context.
    $object = new TestClassSimpleClass();

    $this->service
      ->addContext($object, 'test');

    // Because php passes all obejcts by reference.
    $object->public_property = 'public_property_modified';
    $other_object = $this->service->getContext('test');
    $this->assertEquals('public_property_modified', $other_object->public_property);
  }

  /**
   * @covers DataService::getValue
   */
  public function testGetValue() {
    $object = new TestClassSimpleClass();
    $this->service
      ->addContext($object, 'test');
    $value = $this->service->getValue('test.public_property');
    $this->assertEquals('public', $value);
  }

  /**
   * @covers DataService::selectContext()
   */
  public function testSelectContext() {
    // Simulate an object
    $object = new TestClassSimpleClass();

    // Simulate an array like one used to retrieve data from databases/json
    $array = [
      ['color' => 'blue'],
      ['color' => 'red'],
    ];

    // Load up two contexts.
    $this->service
      ->addContext($object, 'first_context');
    $this->service
      ->addContext($array, 'second_context');

    $this->service
      ->selectContext('first_context');

    // Make sure we can get the current context.
    $value = $this->service->getValue('public_property');
    $this->assertEquals(
      'public',
      $value
    );
  }

}
