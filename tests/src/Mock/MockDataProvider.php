<?php
/**
 * Created by PhpStorm.
 * User: David.Metzler
 * Date: 5/25/2018
 * Time: 5:46 PM
 */

namespace Forena\Tests\Mock;


use Forena\Data\DataProviderBase;

class MockDataProvider extends DataProviderBase
{

  public $access = TRUE;

  public function access($path)
  {
    return $this->access;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($path)
  {
    return new TestClassSimpleClass();
  }
}