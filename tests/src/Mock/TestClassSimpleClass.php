<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/23/17
 * Time: 5:50 PM
 */

namespace Forena\Tests\Mock;

/**
 * Class TestClassSimpleClass
 *
 * Mock class for testing object accessing.
 */
class TestClassSimpleClass {
  public $public_property = "public";
  public $message="Hello World!";
  public $colors = [
    ['color' => 'red'],
    ['color' => 'yellow'],
    ['color' => 'blue'],
  ];
  protected $protected_property="protected";
}
