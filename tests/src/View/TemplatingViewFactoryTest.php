<?php

namespace Forena\Tests\View;

use Forena\Tests\ForenaUnitTestCase;
use Forena\View\TemplatingViewFactory;

/**
 * TemplatingViewFactory Test Case
 * @package Forena\Tests\View
 * @coversDefaultClass TemplatingViewFactory
 */
class TemplatingViewFactoryTest extends ForenaUnitTestCase {

  /** @var  TemplatingViewFactory */
  protected $factory;

  public function setUp() {
    parent::setUp();
    $this->factory = TemplatingViewFactory::create($this->test_dir . '/templates');
  }

  /**
   * Test creation of factory
   * @covers TemplatingViewFactory::create()
   * @covers TemplatingViewFactory::service()
   */
  public function testCreate() {
    $this->assertInstanceOf(TemplatingViewFactory::class, $this->factory);
    $this->assertEquals($this->factory, TemplatingViewFactory::service());
  }

  /**
   * Test HTML Template method
   * @covers TemplatingViewFactory::HTMLTemplate()
   */
  public function testHTMLTemplate() {
    $view = $this->factory->HTMLTemplate('test.html');
    $text = $view->show();
    $this->assertContains('Hello World', $text);
  }

  public function testFrxTemplate() {
    $view = $this->factory->FrxTemplate('test.frx');
    $text = $view->show();
    $this->assertContains('Hello World', $text);
  }

}