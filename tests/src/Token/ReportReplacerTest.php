<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 9/29/2017
 * Time: 10:48 AM
 */

namespace Forena\Tests\Token;

use Forena\Data\DataService;
use Forena\Tests\ForenaUnitTestCase;
use Forena\Tests\Mock\TestClassSimpleClass;
use Forena\Token\ReportReplacer;

class ReportReplacerTest extends ForenaUnitTestCase {

  // Test a simple text replacement.
  public function testTokenReplace() {
    $replacer = new ReportReplacer();
    $data = new TestClassSimpleClass();
    $service = DataService::service();
    $service->addContext($data, 'test');

    /** @var string $text */
    $text = $replacer->replace("Message: {message}");
    $this->assertEquals("Message: Hello World!", $text);

    $a = [
      'text' => '{message}',
    ];
    $replacer->replaceNested($a);
    $this->assertEquals(['text' => 'Hello World!'], $a);
  }
}