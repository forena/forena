<?php


namespace Forena\Tests\RDB;


use Doctrine\DBAL\Events;
use Forena\RDB\Database;

/**
 * Class TestDatabase
 * Used for database testing against postgres.
 */
class TestDatabase extends Database {

  const DEFAULT_DB_CONFIG = [
    'url' => 'postgres://forena@localhost:5445/forena',
    'password' => 'forena'
  ];

  public function __construct() {
    parent::__construct();
    $this->eventManager->addEventListener(Events::postConnect,  $this);
  }

  const DEFAULT_CONFIG_PREFIX = 'FORENA';

  public $connected = FALSE;

  public function postConnect() {
    $this->connected = TRUE;
  }

}