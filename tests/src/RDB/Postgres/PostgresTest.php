<?php


namespace Forena\Tests\RDB\Postgres;




use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Forena\Tests\RDB\TestDatabase;
use PHPUnit\Framework\TestCase;

/**
 * Class PostgresTest
 */
class PostgresTest extends TestCase {

  /** @var TestDatabase */
  protected $db;
  /** @var Connection */
  protected $connection;

  /**
   * @throws DBALException
   */
  public function setUp() {
    parent::setUp();
    $this->db = new TestDatabase();
    $this->connection = $this->db->getConnection();
    $this->connection->executeUpdate("CREATE TABLE IF NOT EXISTS forena_test_data(id INTEGER, data VARCHAR(512))");
    $this->db->beginTransaction();
  }

  /**
   * @throws \Doctrine\DBAL\DBALException
   */
  public function testInsert() {
    $this->connection->executeUpdate("INSERT INTO forena_test_data(id, data) VALUES (1, 'foo')");
    $rows = $this->connection->query("SELECT * FROM forena_test_data")->rowCount();
    $this->assertNotEmpty($rows);

  }

  public function testEventSubsribe() {
    // Test to make sure the test database listener method fired.
    $this->assertTrue($this->db->connected);
  }

  /**
   * @throws \Doctrine\DBAL\DBALException
   */
  public function testRollback() {
    $rows = $this->connection->query("SELECT * FROM forena_test_data")->rowCount();
    $this->assertEmpty($rows);
  }

  /**
   * @throws \Doctrine\DBAL\ConnectionException
   */
  public function tearDown() {
    $this->db->rollback();
    parent::tearDown();
  }


}