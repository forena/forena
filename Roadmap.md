# Features

## Reporting
* User defined data connections
* List reports by those who have access
* User editable reports
* User editable SQL data blocks
* Graphing and charting via PHPSvgGraph
* PDF generation via Prince
* PDF generation via Documentor
* Excel Exports 
* CSV Exports 
* Word Exports 
* Email Merge 
* Crosstab queries. 

## Programing features 
* Dynamic SVG creation. 
* Parameter form definition. 
* Custom Rendererers and controls. 
* Xpath expressions
* Calculated fields
* Custom Data Context defninition. 
* Grouping
* JSON Data services.
* Templated JSON 
* Reports as blocks. 
* Custom Formatters. 
* Forena as a feeds source. 

